FROM node:10-alpine

LABEL maintainer="Bartosz Myszke <bmyszke@gmail.com>"

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN npm install
# Bundle app source
COPY . .

# RUN npm run start:server

EXPOSE 3000

CMD [ "npm", "run", "start:server" ]
