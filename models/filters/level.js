const mongoose = require('mongoose');

const levelSchema = mongoose.Schema({
  name: { type: String, required: true },
  url: { type: String, required: true }
});

module.exports = mongoose.model('LevelFilter', levelSchema);
