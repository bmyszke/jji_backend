const mongoose = require('mongoose');

const techSchema = mongoose.Schema({
  name: { type: String, required: true },
  url: { type: String, required: true },
  icon: { type: String, required: true }
});

module.exports = mongoose.model('TechFilter', techSchema);
