const mongoose = require('mongoose');

const placeSchema = mongoose.Schema({
  city: { type: String, required: true },
  url: { type: String, required: true },
  coordinates: { type: [Number], required: true }
});

module.exports = mongoose.model('PlaceFilter', placeSchema);
