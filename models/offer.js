const mongoose = require('mongoose');

const skillSchema = mongoose.Schema({ 
  name: { type: String, required: true },
  leveltitle: { type: String, required: true },
  levelnumber: { type: String, required: true }
});
const placeSchema = mongoose.Schema({ 
  city: { type: String, required: true },
  street: { type: String, required: true },
  houseNumber: { type: String, required: true }
});

const offerSchema = mongoose.Schema({
  position: { type: String, required: true },
  salaryMin: { type: Number, required: true },
  salaryMax: { type: Number, required: true },
  company: { type: String, required: true },
  size: { type: Number, required: true },
  place: placeSchema,
  technologies: { type: String, required: true },
  levels: { type: [String], required: true },
  url: { type: String, required: true },
  coordinates: { type: [Number], required: true },
  logo: { type: String, required: true },
  emptype: { type: String, required: true },
  shortdesc: { type: String, required: true },
  skills: [skillSchema],
  description: { type: String, required: true },
  creator: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true }
});

module.exports = mongoose.model('Offer', offerSchema);