const Offer = require("../models/offer");

exports.createOffer = (req, res, next) => {
    const url = req.protocol + "://" + req.get("host");   
    const offer = new Offer({
      ...req.body,   
      logo: url + "/images/" + req.file.filename,   
      creator: req.userData.userId
    });    
    offer.save().then(createdOffer => {
      res.status(201).json({
        message: "Offer added successfully",
        offerId: createdOffer._id
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Creating an offer failed!"
      });
    });
  }

  exports.getOffers = (req, res, next) => {
    Offer.find().then(documents => {
      res.status(200).json({
        message: "Offers fetched successfully!",
        offers: documents
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Fetching offers failed!"
      });
    });
  }

  exports.deleteOffer = (req, res, next) => {
    Offer.deleteOne({ _id: req.params.id, creator: req.userData.userId }).then(result => {     
      
      if (result.n > 0) {
        res.status(200).json({ message: "Deletion successful!" });
      } else {
        res.status(401).json({ message: "Not authorized!" });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "Deleting posts failed!"
      });
    });
  }

  exports.getOffer = (req, res, next) => {
    Offer.findById(req.params.id).then(offer => {
      if (offer) {
        offer.id = offer._id;
        delete offer._id;
        res.status(200).json(offer);
      } else {
        res.status(404).json({ message: "Offer not found!" });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "Fetching offer failed!"
      });
    });
  }

  exports.updateOffer = (req, res, next) => {
    let logo = req.body.logo;
    if (req.file) {
      const url = req.protocol + "://" + req.get("host");
      logo = url + "/images/" + req.file.filename;
    }
    const offer = new Offer({
        ...req.body,
      _id: req.body.id,     
      logo: logo,
      creator: req.userData.userId
    });
    Offer.updateOne({ _id: req.params.id, creator: req.userData.userId}, offer).then(result => {
      if (result.nModified > 0) {
        res.status(200).json({ message: "Update successful!" });
      } else {
        res.status(401).json({ message: "Not authorized!" });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "Couldn't udpate post!"
      });
    });
  }

  exports.getOfferByUrl = (req, res, next) => {
    Offer.findOne({url :req.params.url} ).then(offer => {
      if (offer) {
        offer.id = offer._id;
        delete offer._id;
        res.status(200).json(offer);
      } else {
        res.status(404).json({ message: "Offer not found!" });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "Fetching an offer failed!"
      });
    });
  }