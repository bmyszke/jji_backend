const express = require("express");

const OfferController = require("../controllers/offers");

const checkAuth = require("../middleware/check-auth");
const extractFile = require("../middleware/files");

const router = express.Router();


router.post("", checkAuth, extractFile, OfferController.createOffer);

router.get("", OfferController.getOffers);

router.delete("/:id", checkAuth, OfferController.deleteOffer);

router.get("/:id", OfferController.getOffer);


router.put("/:id", checkAuth, extractFile, OfferController.updateOffer);

router.get("/url/:url", OfferController.getOfferByUrl);

module.exports = router;